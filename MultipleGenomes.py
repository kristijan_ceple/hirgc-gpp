import argparse

help_string = """
If compressing, pass reference and target paths(folders)
If decompressing, pass reference path(folder) and target paths(.7z zip-files)
"""
print(help_string)

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--program_name", type=str,
                    help="Name of the program executable.",
                    default="HiRGC_MyImplementation.exe")
parser.add_argument("way", type=str,
                    help="Compression or decompression.", choices=["compr", "decompr"])
parser.add_argument("reference", type=str,
                    help="Reference path.")
parser.add_argument("targets", type=str,
                    help="The list of target genomes.",
                    nargs='+')
args = parser.parse_args()

# Now need to form the command
# First the command base
commandBase = args.program_name + ' ' + args.way + ' -r ' + args.reference + ' -t '
# print(commandBase)

# Now for each target entry do the genome - genome compression
for targetGenome in args.targets:
    commandToExec = commandBase + targetGenome + ' -o res_' + targetGenome
    print(commandToExec)
