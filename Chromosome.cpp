#include "Chromosome.h"
#include "Util.h"
#include "HashTable.h"
#include <fstream>
#include <array>
#include <boost/algorithm/string/trim.hpp>
#include <fmt/core.h>
#include <unordered_map>
#include <variant>
#include <chrono>
using namespace std;
using namespace std::chrono;

/*
Basically used during decompression to construct a new chromo that will be restored.
*/
Chromosome::Chromosome(std::vector<uint8_t>& refSequence, std::string& name)
	: refSequence{ refSequence }, name{ name }
{ 
	this->initMemory();
}

/*
Initialises path and custom name.
*/
Chromosome::Chromosome(string& path, vector<uint8_t>& refSequence, string& name)
	: name{ name }, refSequence{ refSequence }
{
	this->initMemory();
	bool success = this->loadTarFile(path);

	if (!success) {
		throw runtime_error("Chromosome file wasn't read successfully!");
	}
	
}

/*
Initialises path, and uses a set of predefined names.
*/
Chromosome::Chromosome(string& path, vector<uint8_t>& refSequence, uint8_t defaultChromosomeNamesCounter)
	: refSequence{ refSequence }
{
	this->name = "chr" + to_string(defaultChromosomeNamesCounter) + ".fa";
	this->initMemory();
	bool success = this->loadTarFile(path);

	if (!success) {
		throw runtime_error("Chromosome file wasn't read successfully!");
	}
}

/*
Initialises variable lengths and sizes!
*/
void Chromosome::initMemory()
{
	if (!PREDEFINED_SIZES) {
		return;
	}

	this->refSequence.reserve(MAX_CHR_LEN);
	this->tarSequence.reserve(MAX_CHR_LEN);

	this->lwcIntervals.reserve(MIN_SIZE);
	this->NIntervals.reserve(MIN_SIZE);
	this->otherCharacters.reserve(MIN_SIZE);
	this->lineBreakPositions.reserve(MIN_SIZE);
	this->lineBreakPositionsRunLength.reserve(MIN_SIZE);

	this->greedyMatchingResults.reserve(MIN_SIZE);
}

/*
Note that i and wholeSeqIndex can be interchanged(due to recording linebreaks). I'll be using i
in order to make it smaller.
*/
bool Chromosome::loadTarFile(const string& path)
{
	auto loadStart = high_resolution_clock::now();

	ifstream readFile(path);
	if (!readFile.is_open()) {
		return false;
	}

	// ###################################		WRAP TEXT HELPER VARS		###################################################
	string line;
	unsigned int wholeSeqIndex = 0;					// Have to wrap the text around, keep track of whole sequence index -- no need for that since will be recording linebreak positions

	// int currLwcIntervalLen = 0;
	Interval currLwcInterval;
	bool prevUpperCase = true;					// Act as if everything is normal! If another upper comes - OK, if lower comes a new interval starts!

	// int currNIntervalLen = 0;
	Interval currNInterval;
	bool prevN = false;							// Act as if everything is normal! If no N comes - okay. And if N comes - start a new interval!

	char toProcess = ' ';
	// ###################################		WRAP TEXT HELPER VARS		###################################################
	
	unsigned int lineCounter = -1;
	while (getline(readFile, line)) {
		++lineCounter;
		
		if (line.empty()) {
			this->lineBreakPositions.push_back(0);
			continue;
		}

		// Wanna eliminate the \r at the end
		if (line.back() == '\r') {
			// Pop the \r, it interferes
			line.pop_back();
		}

		// First trim, then record number of items in line
		boost::trim_right(line);		// This is maybe unnecessary(by the definition of a FASTA file)

		// Check if comment or header ('>' and ';' signs) and skip them!	--do not record line breakpoint position
		toProcess = line.front();
		if (toProcess == '>') {
			if (lineCounter == 0) {
				this->fastaIdentifier = line;			// Add this line as fasta identifier
			}
			else {
				this->comments.emplace_back(
					Comment{
						lineCounter,
						line
					}
				);
			}
			continue;
		}
		else if (toProcess == ';') {
			this->comments.emplace_back(
				Comment{
					lineCounter,
					line
				}
			);
			continue;
		}

		this->lineBreakPositions.push_back(line.length());		// Record the linebreak position in this line post-trim

		// Read char by char, pack them into the struct TinyNums, and form a vector of them!
		unsigned long long n = line.size();
		for (unsigned long long i = 0; i < n; i++, wholeSeqIndex++) {
			toProcess = line[i];

			// Check if upper-case
			if (islower(toProcess)) {
				// Either extend the current interval, or start a new interval
				if (prevUpperCase) {
					// e.g.: At
					// Have to begin a new interval
					currLwcInterval.pos = wholeSeqIndex;						// Start beginning position
					currLwcInterval.len = 1;						// Reset length
				}
				else {
					// e.g. at
					// Have to continue the last interval
					currLwcInterval.len++;					// Continue interval
				}

				prevUpperCase = false;
				toProcess = toupper(toProcess);
			}
			else {
				// Upper case character, check if perhaps an interval should end
				if (!prevUpperCase) {
					// e.g. aA
					// Upper case after lower case -> end the interval!
					// currLwcInterval.len = currLwcIntervalLen;			// Record len, beginning pos already recorded 
					this->lwcIntervals.push_back(currLwcInterval);		// Copy the interval to the vector of intervals
					prevUpperCase = true;
				}

				// e.g. AA - prevUpperCase is well set, no need to do anything!
			}

			// Okay, survived the lowercase part. Time to mvoe on to N intervals!
			if (toProcess == 'N') {
				if (prevN) {
					// Continue interval
					currNInterval.len++;								// Elongate the curr interval
				}
				else {
					// Start a new interval
					currNInterval.pos = wholeSeqIndex;				// Record start pos
					currNInterval.len = 1;							// Reset length to 1
					prevN = true;
				}

				// Since it's N, we can just continue, no need for further processing
				continue;
			}
			else {
				// Have to check now if maybe we're ending an N interval
				if (prevN) {
					// e.g. 'NA', end the interval!
					// currNInterval.len = currNIntervalLen;			// Pos already recorded, now record length
					// currNIntervalLen = 1;							// Reset length
					this->NIntervals.push_back(currNInterval);		// Add this interval
					prevN = false;									// Reset the flag for this character
				}

				// either e.g. 'NA' or 'AG'
				// Nothing to do here!, 'NA' case already covered, and 'AG' doesn't need anything
			}

			// After checking for 'N' move on to all other characters except legal ones
			uint8_t toCheck = basesFunc(toProcess);
			if (toCheck != 4) {
				// Success! Now add them to the target sequence!
				this->tarSequence.push_back(toCheck);
			}
			else {
				// OtherCharacter!
				OtherChar toAdd{ wholeSeqIndex, toProcess };
				this->otherCharacters.push_back(toAdd);
			}
		}
	}

	// Have to check for any unclosed intervals, in case they weren't finished
	// Need to check lower case chars -- check if last was lwc. Then check for letters N

	if (!prevUpperCase) {
		// It was accounted for, just need to push the Interval up
		this->lwcIntervals.push_back(currLwcInterval);
		toProcess = toupper(toProcess);
	}

	// Do the same for N
	if (toProcess == 'N') {
		this->NIntervals.push_back(currNInterval);
	}

	// Other characters are insta-pushed, so that's okay! Should be done now!
    cout << "Read the target file << " << path << "!" << '\n';
	readFile.close();

	auto loadStop = high_resolution_clock::now();
	auto duration = duration_cast<seconds>(loadStop - loadStart);
	cout << "Target sequence loading duration: " << duration.count() << " seconds!" << '\n';

	return true;
}

void Chromosome::greedyMatching(std::vector<std::vector<unsigned int>*>& hashTable)
{
	// ###########################################			Cache			###################################################
	unsigned int ref_n = this->refSequence.size();
	unsigned int tar_n = this->tarSequence.size();
	unsigned int tar_n_minus_const_k = tar_n - CONST_K;
	vector<unsigned int>* bucket = nullptr;

	array<uint8_t, CONST_K> buffer;
	Interval maxInterval = {0, 0};
	// ###########################################			Cache			###################################################
	// Need to preprocess the first tuple(-- fill the buffer)
	//for (uint8_t counter = 0; counter < CONST_K; counter++) {
	//	buffer[counter] = this->tarSequence[counter];
	//}
	//unsigned long long sum = 0;
	//for (uint8_t i = 0; i < CONST_K; i++) {
	//	sum += ((unsigned long long)refSequence[i]) << tupleCoeffsCached[i];
	//}

	unsigned int tar_seq_index = 0;
	for (; tar_seq_index <= tar_n_minus_const_k; tar_seq_index++) {
		// Let's start -> take the tuple of length k from the target sequence
		for (uint8_t counter = 0; counter < CONST_K; counter++) {
			buffer[counter] = this->tarSequence[tar_seq_index + counter];
		}

		// Filled the buffer of at least k characters, now calculate the tuple value and find the list in the hash table
		// If no entry in the hash table, add this character as a mismatch, and move on
		unsigned int tupleHash = TupleValue::calculateTupleHash(buffer);

		// Got the value - now check if there is such an entry in the hash table
		bucket = hashTable[tupleHash];
		if (bucket == nullptr) {
			// No luck here, move on to the next index, but first save the mismatch!
			this->greedyMatchingResults.emplace_back(this->tarSequence[tar_seq_index]);
			continue;
		}


		/*
		Else we've got some results. Now need to:
		1. Check if any of the tuples actually matches
		2. Find the longest match
		Think I'll do this in one massive monolith function :D
		*/
		vector<Interval> matches;
		for (unsigned int matchStartIndexRef : *bucket) {
			unsigned int counter = 0;
			bool match = true;
			for (unsigned int tar_i = tar_seq_index; tar_i < tar_n; tar_i++, counter++) {
				// Okay, now gotta check them char by char. AT LEAST K chars must match
				// But first check if ref index didn't go outta bounds!
				unsigned int ref_i = matchStartIndexRef + counter;
				if (ref_i == this->refSequence.size()) {
					// Oh well, nothing we can do here, but break the loop and assess the results!
					break;
				}

				if (this->refSequence[ref_i] != this->tarSequence[tar_i]) {
					// Check if counter didn't go far enough, and in case it didn't, move on to next possible-match reference tuple!
					if (counter < CONST_K) {
						match = false;
						break;				// Well, just move on to the next item I guess :(
					}
					else {
						break;				// Guess we're done with this item, move on the next one!
					}
				}
				// If the chars match, go on -> maximise the match! Continue this greedy loop!
			}

			/*
			Some notes:
				1) The target may be logner than the reference
				2) It is possible for the loop to exit due to coming to an end. That's why I've introduced a flag
			*/
			if (match) {
				matches.emplace_back(
					Interval{
						matchStartIndexRef,
						counter
					}
				);
			}
		}

		// All the items in the bucket have been done. Has any even been matched? Which match is the best? Gotta find that out now
		if (matches.size() == 0) {
			// Oh well, no luck at this tuple, just move on to the next tuple/ Add this charcter as a mismatch
			this->greedyMatchingResults.emplace_back(this->tarSequence[tar_seq_index]);
			continue;
		}
		else {
			// Go through all the matches, and find the longest one
			unsigned int maxLen = 0, maxRefPos = 0;
			for (Interval& match : matches) {
				if (match.len > maxLen) {
					maxLen = match.len;
					maxRefPos = match.pos;
				}
			}

			// Okay, now just note it down
			this->greedyMatchingResults.emplace_back(
				Interval
				{
					maxRefPos
					, maxLen
				}
			);

			// Always after a match follows a mismatch!
			// Well, not always actually.
			/*
			NORMAL	1) Normal situation - after a match follows a mismatch
			EDGE	2) End - we arrived to the end of the target sequence
			
			IGNORE	3*) The BEST match stretches till the end of the reference sequence, BUT the target sequence is longer. In this case there's no need
				to note the next character down as a mismatch, since it may form another match.
				BUT -- this is not the way of the HiRGC, so I'll note it down as a mismatch! hehe

				Need to check for the edge case
			*/
			unsigned int mismatchIndex = tar_seq_index + maxLen;
			if (mismatchIndex != this->tarSequence.size()) {
				this->greedyMatchingResults.emplace_back(this->tarSequence[mismatchIndex]);
				// One last thing, need to move the target index
				// tar_seq_index--;		// This is done to counter the loop automatic incrementing ---> Later remembered that after a match always follows a mismatch, so yeah let the loop just skip that 1 boye. btw the edge case should be covered
				tar_seq_index = mismatchIndex;
			}
			else {
				// Covered the whole target sequence -> we're DONE
				this->greedyMatchingRun = true;
				return;
			}
		}
	}
	
	// Jesus this was rough, hope it won't explode too spectacularly(or rather that it will, makes for easy debugging)
	// It can happen that there are some leftover characters -- they must be noted down as mismatches
	for (; tar_seq_index < tar_n; tar_seq_index++) {
		this->greedyMatchingResults.emplace_back(this->tarSequence[tar_seq_index]);
	}

	// Mark that matching has been done
	this->greedyMatchingRun = true;
}

void Chromosome::greedyMatchingOptimised(
	std::vector<std::vector<unsigned int>*>& hashTable
	//, std::array<unsigned int, CONST_K>& tupleCoeffsCached_All
	//, unsigned int tupleCoeffsCached_Last
)
{
	// ###########################################			Cache			###################################################
	unsigned int ref_n = this->refSequence.size();
	unsigned int tar_n = this->tarSequence.size();
	unsigned int tar_n_minus_const_k = tar_n - CONST_K;
	vector<unsigned int>* bucket = nullptr;

	array<uint8_t, CONST_K> buffer;
	unsigned int maxPos = 0;
	unsigned int maxLen = 0;
	// ###########################################			Cache			###################################################
	// Need to preprocess the first tuple(-- fill the buffer)
	//for (uint8_t counter = 0; counter < CONST_K; counter++) {
	//	buffer[counter] = this->tarSequence[counter];
	//}
	//unsigned long long sum = 0;
	//for (uint8_t i = 0; i < CONST_K; i++) {
	//	sum += ((unsigned long long)refSequence[i]) << tupleCoeffsCached[i];
	//}

	unsigned int tar_seq_index = 0;
	for (; tar_seq_index <= tar_n_minus_const_k; tar_seq_index++) {
		// Let's start -> take the tuple of length k from the target sequence
		for (uint8_t counter = 0; counter < CONST_K; counter++) {
			buffer[counter] = this->tarSequence[tar_seq_index + counter];
		}

		// Filled the buffer of at least k characters, now calculate the tuple value and find the list in the hash table
		// If no entry in the hash table, add this character as a mismatch, and move on
		unsigned int tupleHash = TupleValue::calculateTupleHash(buffer);

		// Got the value - now check if there is such an entry in the hash table
		bucket = hashTable[tupleHash];
		if (bucket == nullptr) {
			// No luck here, move on to the next index, but first save the mismatch!
			this->greedyMatchingResults.emplace_back(this->tarSequence[tar_seq_index]);
			continue;
		}


		/*
		Else we've got some results. Now need to:
		1. Check if any of the tuples actually matches
		2. Find the longest match
		Think I'll do this in one massive monolith function :D
		*/
		for (unsigned int matchStartIndexRef : *bucket) {
			unsigned int counter = 0;
			bool match = true;
			for (unsigned int tar_i = tar_seq_index; tar_i < tar_n; tar_i++, counter++) {
				// Okay, now gotta check them char by char. AT LEAST K chars must match
				// But first check if ref index didn't go outta bounds!
				unsigned int ref_i = matchStartIndexRef + counter;
				if (ref_i == this->refSequence.size()) {
					// Oh well, nothing we can do here, but break the loop and assess the results!
					break;
				}

				if (this->refSequence[ref_i] != this->tarSequence[tar_i]) {
					// Check if counter didn't go far enough, and in case it didn't, move on to next possible-match reference tuple!
					if (counter < CONST_K) {
						match = false;
						break;				// Well, just move on to the next item I guess :(
					}
					else {
						break;				// Guess we're done with this item, move on the next one!
					}
				}
				// If the chars match, go on -> maximise the match! Continue this greedy loop!
			}

			/*
			Some notes:
				1) The target may be logner than the reference
				2) It is possible for the loop to exit due to coming to an end. That's why I've introduced a flag
			*/
			if (match) {
				if (counter > 0) {
					if (counter > maxLen) {
						maxLen = counter;
						maxPos = matchStartIndexRef;
					}
				}
			}
		}

		// All the items in the bucket have been done. Has any even been matched? Which match is the best? Gotta find that out now
		if (maxLen == 0) {
			// Oh well, no luck at this tuple, just move on to the next tuple/ Add this charcter as a mismatch
			this->greedyMatchingResults.emplace_back(this->tarSequence[tar_seq_index]);
			continue;
		}
		else {
			// Okay, now just note it down
			this->greedyMatchingResults.emplace_back(
				Interval
				{
					maxPos
					, maxLen
				}
			);

			// Always after a match follows a mismatch!
			// Well, not always actually.
			/*
			NORMAL	1) Normal situation - after a match follows a mismatch
			EDGE	2) End - we arrived to the end of the target sequence

			IGNORE	3*) The BEST match stretches till the end of the reference sequence, BUT the target sequence is longer. In this case there's no need
				to note the next character down as a mismatch, since it may form another match.
				BUT -- this is not the way of the HiRGC, so I'll note it down as a mismatch! hehe

				Need to check for the edge case
			*/
			unsigned int mismatchIndex = tar_seq_index + maxLen;
			if (mismatchIndex != this->tarSequence.size()) {
				this->greedyMatchingResults.emplace_back(this->tarSequence[mismatchIndex]);
				// One last thing, need to move the target index
				// tar_seq_index--;		// This is done to counter the loop automatic incrementing ---> Later remembered that after a match always follows a mismatch, so yeah let the loop just skip that 1 boye. btw the edge case should be covered
				tar_seq_index = mismatchIndex;
			}
			else {
				// Covered the whole target sequence -> we're DONE
				this->greedyMatchingRun = true;
				return;
			}

			maxLen = 0;			// Have to reset the var
		}
	}

	// Jesus this was rough, hope it won't explode too spectacularly(or rather that it will, makes for easy debugging)
	// It can happen that there are some leftover characters -- they must be noted down as mismatches
	for (; tar_seq_index < tar_n; tar_seq_index++) {
		this->greedyMatchingResults.emplace_back(this->tarSequence[tar_seq_index]);
	}

	// Mark that matching has been done
	this->greedyMatchingRun = true;
}

void Chromosome::runLengthEncodingLBrks()
{
	if (this->lineBreakPositions.size() == 0) {
		return;
	}

	// Let's encode this
	unsigned int counter = 0;
	unsigned int* cmpTo = &this->lineBreakPositions[0];
	unsigned int* currLineBreakPos = nullptr;

	unsigned int n = this->lineBreakPositions.size();
	for (unsigned int i = 0; i < n; i++) {
		currLineBreakPos = &this->lineBreakPositions[i];

		// Check if same!
		if (*currLineBreakPos == *cmpTo) {
			counter++;
		}
		else {
			// OOps - they differ! Save the last pair, and initialise the next one!
			this->lineBreakPositionsRunLength.push_back(counter);
			this->lineBreakPositionsRunLength.push_back(*cmpTo);

			// Reset to new values
			cmpTo = currLineBreakPos;
			counter = 1;
		}
	}

	// After the loop always have to add the last interval
	this->lineBreakPositionsRunLength.push_back(counter);
	this->lineBreakPositionsRunLength.push_back(*cmpTo);
}