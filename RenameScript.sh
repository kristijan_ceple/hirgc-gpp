# Takes a folder as argument

if [[ "$#" -ne 1 ]]
then
    echo "Wrong amount of arguments! Please pass the folder name!"
    exit -1
fi

targetFolder="$1"

for counter in {1..24}
do
    mv "$targetFolder/chromosome_$counter.fa" "$targetFolder/chr$counter.fa"
done