counter=1
line=">NC_000001.10 Homo sapiens chromosome 1, GRCh37.p13 Primary Assembly"

regex_string="^>.*chromosome ([0-9]+|X|Y)[^\d]*$"
line2="achromosome 1"

if [[ "$line" =~ $regex_string ]]
then
    echo "Match!"
else
    echo "No match!"
fi