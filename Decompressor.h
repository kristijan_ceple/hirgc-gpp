#pragma once
#include <unordered_map>
#include <vector>
#include <variant>
#include <optional>
#include <filesystem>
#include "Chromosome.h"
#include "Util.h"
#include <optional>

/*
	Compression order:
		0. ACGT mapping
		1. Lower Letters
		2. N intervals
		3. Other characters
	Now gotta do the same, but in reverse!
	
	!!! Therefore !!!:
		0. Other characters and intervals that are sequentially placed in a vector
		1. N Intervals
		2. Lower Letters
		3. *ACGT mapping (no need for this, will make a special func!)
*/
class Decompressor
{
private:
	std::string restorationFolder = restorePath;
	std::vector<uint8_t> prevRefSequence;
	bool referenceLoaded = false;

	std::unordered_map<std::string, std::vector<uint8_t>> decompressedChromosomeNumSequences;
	std::unordered_map<std::string, std::vector<char>> decompressedChromosomeCharSequences;
	std::unordered_map<std::string, std::string> decompressedChromosomeStringSequences;
	std::unordered_map<std::string, Chromosome> decompressedChromosomes;
	//std::regex nameCuttingRegex = std::regex("Babaaa");
	
	bool restoreChromosome(std::string name, std::filesystem::path, std::vector<uint8_t>& refSequence);

	bool restoreNumericSequence(Chromosome& toRestore);
	bool restoreCharacterSequence(Chromosome& toRestore);
	bool restoreString(Chromosome& toRestore);
	bool writeRestoredChr2File(Chromosome& toRestore);
	friend class DataHandler;
public:
	inline std::optional<std::reference_wrapper<std::vector<uint8_t>>> getRestoredNumSequence(std::string chromosomeName);
	inline std::optional<std::reference_wrapper<std::vector<char>>> getRestoredCharSequence(std::string chromosomeName);
	inline std::optional<std::reference_wrapper<std::string>> getRestoredString(std::string chromosomeName);

	// Files part
	bool restoreChromosomeFolder(std::string decomprFolderArg, std::string refSeqPath, std::string restoreFolderArg);
	bool restoreChromosomesFolder_PairRefs(std::string decomprFolderArg = decomprPath, std::string refsFolderArg = refsPath, std::string restoreFolderArg = restorePath);
	bool restoreChromosomesFolder_OneRef(std::string decomprFolderArg = decomprPath, std::string refsPath = "./chr1.fa", std::string restoreFolderArg = restorePath);
	void deZipFolder(std::string deZipPath = decomprPath);
	void deRunLenghtEncoding(std::vector<unsigned int>&, std::vector<unsigned int>&);

	// Used for testing purposes - wrappers for private methods
	inline bool _restoreNumericSequence(Chromosome& toRestore);
	inline bool _restoreCharacterSequence(Chromosome& toRestore);
	inline bool _restoreString(Chromosome& toRestore);
};

inline bool Decompressor::_restoreString(Chromosome& toRestore)
{
	return this->restoreString(toRestore);
}

inline bool Decompressor::_restoreNumericSequence(Chromosome& toRestore)
{
	return this->restoreNumericSequence(toRestore);
}

inline bool Decompressor::_restoreCharacterSequence(Chromosome& toRestore)
{
	return this->restoreCharacterSequence(toRestore);
}

inline std::optional<std::reference_wrapper<std::vector<uint8_t>>> Decompressor::getRestoredNumSequence(std::string name)
{
	auto it = this->decompressedChromosomeNumSequences.find(name);
	if (it != this->decompressedChromosomeNumSequences.end()) {
		return it->second;
	}
	else {
		return std::nullopt;
	}
}

inline std::optional<std::reference_wrapper<std::vector<char>>> Decompressor::getRestoredCharSequence(std::string name)
{
	auto it = this->decompressedChromosomeCharSequences.find(name);
	if (it != this->decompressedChromosomeCharSequences.end()) {
		return it->second;
	}
	else {
		return std::nullopt;
	}
}

inline std::optional<std::reference_wrapper<std::string>> Decompressor::getRestoredString(std::string name)
{
	auto it = this->decompressedChromosomeStringSequences.find(name);
	if (it != this->decompressedChromosomeStringSequences.end()) {
		return it->second;
	}
	else {
		return std::nullopt;
	}
}

inline void Decompressor::deZipFolder(std::string deZipPath)
{
	// Time to decompress it!
#if defined(_WIN64) || defined(_WIN32)
	std::string cmd = fmt::format(DECOMPR_CMD_7ZA_WIN_CUSTOM_EXT, deZipPath, deZipPath);
	system(cmd.c_str());
#elif defined(__unix__)
	std::string cmd = fmt::format(DECOMPR_FORMAT_STR_EXT, deZipPath, deZipPath);
	system(cmd.c_str());
#endif
}