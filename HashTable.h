#pragma once
#include <vector>
#include "Util.h"

class HashTable
{
private:
	std::vector<std::vector<unsigned int>*> header;
public:
	HashTable();
	HashTable(unsigned int size);
	~HashTable();
	inline std::vector<unsigned int>* operator[](const std::array<std::uint8_t, CONST_K>& tuple);
	inline std::vector<unsigned int>* emplace(const std::array<std::uint8_t, CONST_K>& tuple);
	inline std::vector<unsigned int>* emplace(unsigned int bucketIndex);
	inline static unsigned int calculateTupleHash(const std::array<uint8_t, CONST_K>& tuple);
};

inline std::vector<unsigned int>* HashTable::emplace(unsigned int bucketIndex)
{
	std::vector<unsigned int>* it = this->header[bucketIndex];
	if (it == nullptr) {
		std::vector<unsigned int>* toRet = new std::vector<unsigned int>();
		toRet->reserve(MIN_BUCKET_SIZE);
		this->header[bucketIndex] = toRet;
		return toRet;
	}
	else {
		return it;
	}
}

inline std::vector<unsigned int>* HashTable::emplace(const std::array<std::uint8_t, CONST_K>& tuple)
{
	//unsigned int hashValue = calculateTupleHash(tuple);

	unsigned long long hashValue = 0;
	for (uint8_t j = 0; j < CONST_K; j++) {
		hashValue += ((unsigned long long)tuple[j]) << (2 * j);
	}

	hashValue &= (HASH_TABLE_SIZE_MINUS_ONE);

	std::vector<unsigned int>* it = this->header[hashValue];
	if (it == nullptr) {
		std::vector<unsigned int>* toRet = new std::vector<unsigned int>();
		toRet->reserve(MIN_BUCKET_SIZE);
		this->header[hashValue] = toRet;
		return toRet;
	}
	else {
		return it;
	}
}

inline unsigned int HashTable::calculateTupleHash(const std::array<uint8_t, CONST_K>& tuple)
{
	unsigned long long sum = 0;
	for (uint8_t j = 0; j < CONST_K; j++) {
		sum += ((unsigned long long)tuple[j]) << (2 * j);
	}

	return sum & (HASH_TABLE_SIZE_MINUS_ONE);
}

inline std::vector<unsigned int>* HashTable::operator[](const std::array<std::uint8_t, CONST_K>& tuple)
{
	unsigned long long sum = 0;
	for (std::uint8_t j = 0; j < CONST_K; j++) {
		sum += ((unsigned long long)tuple[j]) << (2 * j);
	}

	sum &= (HASH_TABLE_SIZE_MINUS_ONE);
	return this->header[sum];
}