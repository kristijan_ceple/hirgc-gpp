if [[ "$#" != 2 ]]
then
    >&2 echo "Invalid number of args passed!"
    exit -1
fi

file_name="$1"
output_folder="$2"

rm -rf "$output_folder"
mkdir "$output_folder"

regex_string="^>.*chromosome ([0-9]+|X|Y)[^\d]*$"
suffix=""

echo "Regex string: $regex_string"
already_visited=()
while read line
do
    if [[ "$line" =~ $regex_string ]]
    then
        # Check whether this is another part of the same chromosome
        new_suffix=${BASH_REMATCH[1]}

        if [[ "${already_visited[@]}" =~ "$new_suffix" ]]
        then
            # If already visited then append to that file! Set the vars
            suffix="$new_suffix"
            outfile="chr$suffix.fa"
            echo "$line" >> "$output_folder/$outfile"
            continue
        fi

        # If not already visited, make a new file! Set the vars!
        suffix="$new_suffix"
        outfile="chr$suffix.fa"
        already_visited+=("$suffix")

        echo "$line" > "$output_folder/$outfile"
    else
        echo "$line" >> "$output_folder/$outfile"
    fi
done < "$file_name"