#include <iostream>
#include <regex>
#include <chrono>
#include "Util.h"
using namespace std;
using namespace std::chrono;

/**
This method loads a single file(actually should be used to load a single chromosome). Gene loading should be done
using this method called on chr-chr pairs.
*/
bool Util::loadRefFile(string path, vector<uint8_t>& prevRefSeq)
{
	// Check the extension
	if (!checkFastaExtension(path)) {
		return false;
	}
	
	auto loadStart = high_resolution_clock::now();
	ifstream readFile(path);
	if (!readFile.is_open()) {
		return false;
	}

	// First clear the existing reference
	prevRefSeq.clear();
	prevRefSeq.reserve(MAX_CHR_LEN);

	string line;
	int8_t nucleotide;

	// Now check the header, as to increase the speed!
	getline(readFile, line);
	char toProcess = line.front();
	if (! (toProcess == '>' || toProcess == ';') ) {			// Check if comment or header ('>' and ';' signs) and skip them!
		unsigned int n = line.size();
		for (unsigned int i = 0; i < n; i++) {
			char toCheck = line[i];
			nucleotide = basesFunc(toCheck);
			if (nucleotide != 4) {
				prevRefSeq.push_back(nucleotide);
			}
		}
	}

	while (getline(readFile, line)) {
		// Read char by char, pack them into the struct TinyNums, and form a vector of them!
		unsigned int n = line.size();
		for (unsigned int i = 0; i < n; i++) {
			char toCheck = line[i];
			nucleotide = basesFunc(toCheck);
			if (nucleotide != 4) {
				prevRefSeq.push_back(nucleotide);
			}
		}
	}

	cout << "Read the reference file << " << path << "!" << '\n';
	readFile.close();

	auto loadStop = high_resolution_clock::now();
	auto duration = duration_cast<seconds>(loadStop - loadStart);
	cout << "Reference sequence loading duration: " << duration.count() << " seconds!" << '\n';

	return true;
}

void printDurationFromStart(high_resolution_clock::time_point& start, TimeUnit timeUnit)
{
	auto stop = high_resolution_clock::now();
	switch (timeUnit) {
	    case TimeUnit::Minutes: {
            auto durationMins = duration_cast<minutes>(stop - start);
            cout << durationMins.count();
            break;
	    }
        case TimeUnit::Seconds: {
            auto durationSecs = duration_cast<seconds>(stop - start);
            cout << durationSecs.count();
            break;
        }
        case TimeUnit::Microseconds: {
            auto durationMicrosecs = duration_cast<microseconds>(stop - start);
            cout << durationMicrosecs.count();
            break;
        }
	}
}