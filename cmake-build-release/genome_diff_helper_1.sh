#!/bin/bash

for file in GENOME_REF38_TARKOREF_COMPR_*
do
    diff ./GENOME_REF38_TARKOREF_COMPR/compressionRes.7z "$file/compressionRes.7z"
done