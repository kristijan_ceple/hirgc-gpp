#pragma once
#include <cstdint>
#include <array>
#include <chrono>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <fmt/format.h>
#include <regex>
#include <fstream>
#include <boost/functional/hash.hpp>


inline const std::uint8_t CONST_S = 26;

//#define DEBUG
#ifndef DEBUG
inline const unsigned int MAX_CHR_LEN = 1 << 28;
inline const unsigned int HASH_TABLE_SIZE = 1 << CONST_S;
inline const bool PREDEFINED_SIZES = true;
#else
// Used for debugging purposes
inline const unsigned int MAX_CHR_LEN = 1000000;
inline const unsigned int HASH_TABLE_SIZE = 1000000;
inline const bool PREDEFINED_SIZES = false;
#endif

inline const std::uint8_t CONST_K = 2;							// 20 is value recommended by the authors
inline const unsigned int CONST_K_MINUS_1 = CONST_K - 1;			// Caching this for the greedy search algo
inline const unsigned int MAX_CHR_LEN_MINUS_CONST_K_PLUS_1 = MAX_CHR_LEN - CONST_K + 1;
					// Should later try going for 30!
inline const unsigned int HASH_TABLE_SIZE_MINUS_ONE = HASH_TABLE_SIZE - 1;
inline const unsigned int MIN_SIZE = 1 << 20;
inline const std::uint8_t MIN_BUCKET_SIZE = 2;

inline const bool TRAILING_NEWLINE = true;
inline const std::unordered_set<std::string> fastaExtensionsSet = { "fa" , "fasta" , "fna" , "ffn" ,  "faa" , "frn", ".fa", ".fasta" , ".fna" , ".ffn" ,  ".faa" , ".frn" };

// #############################			PROBABLY USELESS			###################################
inline const std::string fastaExtensionsArray[] = { "fa" , "fasta" , "fna" , "ffn" ,  "faa" , "frn" };
inline const std::regex fastaRegex = std::regex(fmt::format(
	R"([[:alpha:]]\w*\.(?:{:s}|{:s}|{:s}|{:s}|{:s}|{:s}))",
	fastaExtensionsArray[0], fastaExtensionsArray[1], fastaExtensionsArray[2],
	fastaExtensionsArray[3], fastaExtensionsArray[4],
	fastaExtensionsArray[5], fastaExtensionsArray[6]
));
// #############################			PROBABLY USELESS			###################################

inline std::string comprPath = "./comprFiles";
inline std::string decomprPath = "./decomprFiles";
inline std::string restorePath = "./restoredFiles";
inline std::string refsPath = "./refsFiles";

// 7zip linux utiliy compresses using PPMD -> first arg(with extension .7z) is the desired name of archive, while the second argument(before -m0) is the name of the folder to be compressed
inline const std::string COMPR_FORMAT_STR = "7za a {0:s}/compressionRes.7z ./{0:s}/* -m0=PPMd";
inline const std::string COMPR_CMD_7ZA_WIN_CUSTOM = fmt::format("bash -c \"{:s}\"", COMPR_FORMAT_STR);				// This one's used in DataHandler
//inline const std::string COMPR_CMD_7ZA_WIN_CUSTOM_INLINED = fmt::format("bash -c \"7za a {0:s}.7z ./{0:s}/* -m0=PPMd\"");				// This one's used in DataHandler
inline const std::string COMPR_CMD_7ZA_LINUX_DEFAULT = fmt::format(COMPR_FORMAT_STR, comprPath);
inline const std::string COMPR_CMD_7ZA_WIN_DEFAULT = fmt::format("bash -c \"{:s}\"", COMPR_CMD_7ZA_LINUX_DEFAULT);

inline const std::string DECOMPR_FORMAT_STR_EXT = "7za e {:s}.7z -o{:s}";
inline const std::string DECOMPR_CMD_7ZA_WIN_CUSTOM_EXT = fmt::format("bash -c \"{:s}\"", DECOMPR_FORMAT_STR_EXT);			// This one's used in DataHandler
inline const std::string DECOMPR_CMD_7ZA_LINUX_DEFAULT_EXT = fmt::format(DECOMPR_FORMAT_STR_EXT, comprPath, decomprPath);
inline const std::string DECOMPR_CMD_7ZA_WIN_DEFAULT_EXT = fmt::format("bash -c \"{:s}\"", DECOMPR_CMD_7ZA_LINUX_DEFAULT_EXT);

inline const std::string DECOMPR_FORMAT_STR_WO_EXT = "7za e {:s} -o{:s}";
inline const std::string DECOMPR_CMD_7ZA_WIN_CUSTOM_WO_EXT = fmt::format("bash -c \"{:s}\"", DECOMPR_FORMAT_STR_WO_EXT);			// This one's used in DataHandler
inline const std::string DECOMPR_CMD_7ZA_LINUX_DEFAULT_WO_EXT = fmt::format(DECOMPR_FORMAT_STR_WO_EXT, comprPath, decomprPath);
inline const std::string DECOMPR_CMD_7ZA_WIN_DEFAULT_WO_EXT = fmt::format("bash -c \"{:s}\"", DECOMPR_CMD_7ZA_LINUX_DEFAULT_WO_EXT);

inline const std::uint8_t HUMAN_GENOME_CHR_NUM = 46;

struct Interval {
	unsigned int pos;
	unsigned int len;
};

struct LetterN {
	unsigned int pos;
	unsigned int len;
};

struct OtherChar {
	unsigned int pos;
	char ch;
};
typedef OtherChar Mismatch;		// Mismatches during greedy searching, just like other characters during target sequence reading and processing, consist of pos and character that was mismatched(this is naturally due to the 99.9% similarity between human DNAs)

struct Comment {
	unsigned int line;
	std::string content;
};

struct TupleValue {
	inline static std::array<uint8_t, CONST_K> tupleCoeffsCached_All;

	inline static bool coeffsInitialised = false;
	
	inline static void initCoeffs(
		const std::array<uint8_t, CONST_K>& tupleCoeffsCached_All
	)
	{
		TupleValue::tupleCoeffsCached_All = tupleCoeffsCached_All;
		TupleValue::coeffsInitialised = true;
	}

	inline static unsigned long long calculateTupleValue(const std::array<uint8_t, CONST_K>& tuple)
	{
		unsigned long long sum = 0;
		for (uint8_t j = 0; j < CONST_K; j++) {
			sum += ((unsigned long long)tuple[j]) << (tupleCoeffsCached_All[j]);
		}

		return sum;
	}

	inline static unsigned int calculateTupleHash(const std::array<uint8_t, CONST_K>& tuple)
	{
		unsigned long long sum = 0;
		for (uint8_t j = 0; j < CONST_K; j++) {
			sum += ((unsigned long long)tuple[j]) << (tupleCoeffsCached_All[j]);
		}

		return sum & (HASH_TABLE_SIZE_MINUS_ONE);
	}
};

struct TupleValueHasher {
	unsigned int operator()(const unsigned long long& k) const
	{
		return k&(HASH_TABLE_SIZE_MINUS_ONE);
	}
};

struct TupleValueHasherBoost {
	std::size_t operator()(const unsigned long long& k) const
	{
		using boost::hash_value;
		return hash_value(k);
	}
};

struct IdentityHasher {
	unsigned int operator()(const unsigned int& k) const
	{
		return k;
	}
};

struct ValidComparator {
	bool operator()(const unsigned int& lhs, const unsigned int& rhs) const
	{
		return true;
	}
};

// inline std::hash<std::array<std::uint8_t, 4>> stdHasher;

struct RestorationVisitor {
private:
	std::vector<uint8_t>& restoredSequence;
	std::vector<uint8_t>& refSequence;
public:
	RestorationVisitor(std::vector<uint8_t>& restoredSequence, std::vector<uint8_t>& refSequence)
		: restoredSequence{ restoredSequence }
		, refSequence{ refSequence }
	{};

	void operator()(uint8_t _in)
	{
		// Just append it to the restored seq
		restoredSequence.push_back(_in);
	}

	void operator()(Interval& _in)
	{
		// This one's a little tougher, need to check against the reference sequence and add those elements
		// Copy len chars [pos, pos+len)
		unsigned int n = _in.pos + _in.len;
		for (unsigned int i = _in.pos; i < n; i++) {
			restoredSequence.push_back(refSequence[i]);
		}
	}
};

struct AUXWriteFileVisitor {
private:
	std::ofstream& ofstr;
public:
	AUXWriteFileVisitor(std::ofstream& ofstr) : ofstr{ ofstr } {};

	void operator()(unsigned int& _in)
	{
		// Append in format <ch>
		ofstr << _in << ' ';
	}

	void operator()(Interval& _in)
	{
		// Append in format <pos,len>
		ofstr << _in.pos << ',' << _in.len << ' ';
	}

	void operator()(OtherChar& _in)
	{
		// Append in format <pos,len>
		ofstr << _in.pos << ',' << _in.ch << ' ';
	}

	void operator()(Comment& _in)
	{
		// Append in format <line string\n>
		ofstr << _in.line << ' ' << _in.content << '\n';
	}
};
typedef AUXWriteFileVisitor AUXVisitor;

struct GreedyWriteFileVisitor {
private:
	std::ofstream& ofstr;
	bool previousUint8_t;
	bool lineBeginning = true;
public:
	GreedyWriteFileVisitor(std::ofstream& ofstr) : ofstr{ ofstr } {};

	void operator()(uint8_t _in)
	{
		if (lineBeginning) {
			// Append in format <ch[ch[ch[ch...]]]>
			ofstr << (unsigned short)_in;
			lineBeginning = false;
		}
		else {
			if (previousUint8_t) {
				ofstr << (unsigned short)_in;
			}
			else {
				// Previous was interval, so append a space
				ofstr << ' ' << (unsigned short)_in;
			}
		}

		previousUint8_t = true;			// Note to self: always set the flag AFTER consuming the former state!!!
	}

	void operator()(Interval& _in)
	{
		if (lineBeginning) {
			ofstr << _in.pos << ',' << _in.len;
			lineBeginning = false;
		}
		else {
			// Append in format <pos,len>
			ofstr << ' ' << _in.pos << ',' << _in.len;
		}

		previousUint8_t = false;
	}
};
typedef GreedyWriteFileVisitor GreedyVisitor;

inline std::string toString(std::vector<uint8_t> sequence, bool printIndices)
{
	fmt::memory_buffer toRet;
	if (printIndices) {
		for (size_t i = 0; i < sequence.size(); i++) {
			fmt::format_to(toRet, "|{:2d}.|", i);
		}
		toRet.push_back('\n');
	}

	std::for_each(sequence.begin(), sequence.end(),
		[&toRet](uint8_t _in) {
			// fmt::format_to(toRet, "|{:^3d}|", _in);
			fmt::format_to(toRet, "{}", _in);
		}
	);

	return toRet.data();
}

inline std::string toString(std::vector<char> sequence)
{
	fmt::memory_buffer toRet;
	std::for_each(sequence.begin(), sequence.end(),
		[&toRet](char _in) {
			fmt::format_to(toRet, "{:c}", _in);
		}
	);

	return toRet.data();
}

struct DecompressedSequence {
	std::vector<uint8_t> numSequence;
	std::vector<char> charSequence;
};

namespace Util {
	bool loadRefFile(std::string path, std::vector<uint8_t>& prevRefSeq);
}

inline int8_t basesFunc(char toCheck)
{
	switch (toCheck) {
	case 'a':
	case 'A':
		return 0;
	case 'c':
	case 'C':
		return 1;
	case 'g':
	case 'G':
		return 2;
	case 't':
	case 'T':
		return 3;
	default:
		return 4;
	}
}

inline char inverseBasesFunc(uint8_t toCheck)
{
	switch (toCheck) {
	case 0:
		return 'A';
	case 1:
		return 'C';
	case 2:
		return 'G';
	case 3:
		return 'T';
	default:
		// Should never happen!
		throw std::invalid_argument("inverseBasesFunc(uint8_t toCheck) must receive 0, 1, 2, or 3 as args!");
	}
}

inline bool checkFastaExtension(std::string path)
{
	unsigned int dotPos = path.find_last_of('.');
	if (dotPos != path.npos) {
		// Check if one of the extensions follows after the dot!
		std::string extension = path.substr(dotPos);

		auto found = fastaExtensionsSet.find(extension);
		if (found == fastaExtensionsSet.end()) {
			return false;		// Extension wrong
		} else{
		    return true;        // Extension right
		}
	} else{
	    return false;           // No Extension
	}
}

inline void cutExtension(std::string& path)
{
	unsigned int dotPos = path.find_last_of('.');

	//std::cout << "dotPos = " << dotPos << std::endl;
	//std::cout << "string::npos = " << std::string::npos << std::endl;

	if (dotPos != (unsigned int)-1) {
		path.erase(dotPos);
	}
}

enum class TimeUnit { Minutes, Seconds, Microseconds };
void printDurationFromStart(std::chrono::high_resolution_clock::time_point& start, TimeUnit timeUnit = TimeUnit::Microseconds);

// ############################################################################################################################################
//#########################################################		LEGACY DATA STRUCTS			###################################################
// ############################################################################################################################################

//inline const std::unordered_map<char, uint8_t> basesMap = {
//	{'a', 0},
//	{'c', 1},
//	{'g', 2},
//	{'t', 3},
//	{'A', 0},
//	{'C', 1},
//	{'G', 2},
//	{'T', 3},
//	//{'N', 4},
//	//{'n', 5}
//};
//
//inline const std::unordered_map<uint8_t, char> inverseBasesMap = {
//	{0, 'A'}
//	,{1, 'C'}
//	,{2, 'G'}
//	,{3, 'T'}
//};
//
//// What even?
//inline const std::array<char, 8> basesArray = {
//	'A', 'C', 'G', 'T', 'a', 'c', 'g', 't'
//};

// ############################################################################################################################################
//#########################################################		LEGACY DATA STRUCTS			###################################################
// ############################################################################################################################################