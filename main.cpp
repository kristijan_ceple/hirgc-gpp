// Entry point for the application
#include <iostream>
#include <variant>
#include <chrono>
#include <algorithm>
#include <string>
#include <filesystem>
#include <fmt/format.h>
#include "Compressor.h"
#include "Decompressor.h"
using namespace std;
using namespace std::chrono;
namespace fs = std::filesystem;

#include <boost/program_options.hpp>
#include "DataHandler.h"
using namespace boost::program_options;

void compression(string& ref, string& tar, string& out, string& mode, string& genomeReferenceMode);
void decompression(string& ref, string& tar, string& out, string& mode, string& genomeReferenceMode);

int main(int argc, char* argv[])
{
    high_resolution_clock::time_point programStart = high_resolution_clock::now();

	try {
		string mode;
		string reference;
		string target;
		string output;
		string way;
		string genomeReferenceMode;

		options_description desc("Allowed options");
		desc.add_options()
			// First parameter describes option name/short name
			// The second is parameter to option
			// The third is description
			("way,w", value(&way), "Compression or decompression! Possible options: compr, decompr")
			("help,h", "Print usage message!")
			("mode,m", value(&mode), "Mode - chromosome, genome, or genome set! Possible options: chr, g, g_set")
			("genome_ref_mode,g", value(&genomeReferenceMode)->default_value("pairwise"), "The way reference sequences will be handled during genome compression. Options are: pairwise, one")
			("reference,r", value(&reference), "Reference chromosome file, genome folder or genome .txt file path!")
			("target,t", value(&target), "Target chromosome file, genome folder or genome .txt file path!")
			("output,o", value(&output)->default_value("./hirgc_out/"), "Output folder path!")
			("number_of_chrs,n", value<int>(&(DataHandler::chrNum))->default_value(HUMAN_GENOME_CHR_NUM), "Used for --mode g. The number of chromosome pairs that will be compressed. Default to 46(the number of human chromosomes).")
			;

		positional_options_description p;
		p.add("way", 1);

		variables_map vm;
		store(command_line_parser(argc, argv).
			options(desc).positional(p).run(), vm);
		notify(vm);

		if (vm.count("help") || argc == 1) {
			cout << "Usage: options_description [options]\n";
			cout << desc;
			return 0;
		}

		cout << "Way = " << way << '\n';
		cout << "Mode = " << mode << '\n';
		cout << "Reference = " << reference << '\n';
		cout << "Target = " << target << '\n';
		cout << "Output folder = " << output << '\n';
		cout << "Genome Reference Mode = " << genomeReferenceMode << '\n';
        cout << "Const S = " << to_string(CONST_S) << '\n';
        cout << "Const K = " << to_string(CONST_K) << '\n';

		// First check the mode
		if (way == "compr") {
			compression(reference, target, output, mode, genomeReferenceMode);
		}
		else if (way == "decompr") {
			decompression(reference, target, output, mode, genomeReferenceMode);
		}
		else {
			cerr << "Invalid mode specified! Please check usage instructions using the --help flag!" << '\n';
			return -1;
		}
	}
	catch (exception& e) {
		cerr << e.what() << "\n";
		return -1;
	}

    high_resolution_clock::time_point programEnd = high_resolution_clock::now();
	cout << "Total runtime: ";
	printDurationFromStart(programStart, TimeUnit::Seconds);
	cout << " seconds!" << '\n';

	return 0;
}

void compression(string& ref, string& tar, string& out, string& mode, string& genomeReferenceMode)
{
	DataHandler dh;

	// First remove the previous folder, and create a new folder!
	fs::remove_all(out);
	fs::create_directory(out);

	if (mode == "chr") {
		dh.chromosomeCompression(ref, tar, out);
	}
	else if (mode == "g") {
		if (genomeReferenceMode == "pairwise") {
			dh.genomeCompression_PairRefs(ref, tar, out);
		}
		else if (genomeReferenceMode == "one") {
			dh.genomeCompression_OneRef(ref, tar, out);
		}
		else {
			cerr << "Invalid genome reference mode specified!" << '\n';
			exit(-1);
		}
	}
	else if (mode == "g_set") {
		dh.genomeSetCompression(ref, tar, out);
	}
	else {
		cerr << "Unsupported mode parameter!" << '\n';
	}
}

void decompression(string& ref, string& tar, string& out, string& mode, string& genomeReferenceMode)
{
	DataHandler dh;

	// Remove former Restored Files folder first, and create a new one!
	fs::remove_all(out);
	fs::create_directory(out);

	if (mode == "chr") {
		dh.chromosomeDeCompression(ref, tar, out);
	}
	else if (mode == "g") {
		if (genomeReferenceMode == "pairwise") {
			dh.genomeDeCompression_PairRefs(ref, tar, out);
		}
		else if (genomeReferenceMode == "one") {
			dh.genomeDeCompression_OneRef(ref, tar, out);
		}
		else {
			cerr << "Invalid genome reference mode specified!" << '\n';
			exit(-1);
		}
	}
	else if (mode == "g_set") {
		dh.genomeSetDeCompression(ref, tar, out);
	}
	else {
		cerr << "Unsupported mode parameter!" << '\n';
	}

	// Remove intermediary directories
	fs::remove_all(tar);
}

// ################################################################################################################################################################
// ####################################################				LEGACY TEST CODE			###################################################################
// ################################################################################################################################################################
// ####################################################				LEGACY TEST CODE			###################################################################
// ################################################################################################################################################################

//void test2DecomprRef(string);
//void test2DecomprRefChar(string);
//void test3WriteFiles(string);
//void test4DecompressFiles();
//void test5DecompressFilesChromosomes();

//int mainTester(void)
//{
//	start = high_resolution_clock::now();
//
//	cout << "Start of program!" << '\n';
//	//test2DecomprRef("ref_test_4");
//	//test2DecomprRef("example_2_ref.txt");
//	//test2DecomprRef("example_1_ref.txt");
//
//	//vector<string> refs = { "ref_test_4" , "example_2_ref.txt" , "example_1_ref.txt" };
//	//for (string& ref : refs) {
//	//	test2DecomprRefChar(ref);
//	//}
//
//	//// Let's try writing some chromos to file
//	//for (string& ref : refs) {
//	//	test3WriteFiles(ref);
//	//}
//
//	//vector<unsigned int> testVec;
//	//cout << testVec.max_size() << '\n';
//	//cout << testVec.size() << '\n';
//	//cout << testVec.capacity() << '\n';
//
//	//testVec.push_back(5);
//	//cout << testVec.capacity() << '\n';
//
//	//test3WriteFiles("ref_test_4.fa");
//	test3WriteFiles("GRCh37_chr1.fna");
//	//test3WriteFiles("3_ref_3.fa");
//
//	test4DecompressFiles();
//	test5DecompressFilesChromosomes();
//
//	// Print total runtime!
//
//	cout << "Total runtime: ";
//	printDurationFromStart(start, TimeUnit::Seconds);
//	cout << " seconds!" << '\n';
//
//	return 0;
//}
//
//void test4DecompressFiles()
//{
//	Decompressor decompr;
//	decompr.deZipFolder();
//}
//
//void test5DecompressFilesChromosomes()
//{
//	Decompressor decompr;
//	decompr.deZipFolder();
//
//	// Now restore the folder!
//	decompr.restoreChromosomesFolder(decomprPath, refsPath);
//}
//
//void test3WriteFiles(string ref)
//{
//	Compressor compr;
//	Decompressor decompr;
//
//	// Let's try decompression now
//	compr.loadRefFile(ref);
//	compr.preprocessRef();
//	cout << "Loaded and preprocessed ref!" << '\n';
//	cout << "Reference: " << ref << '\n' << '\n';
//	
//	cout << "Preprocessing time: ";
//	printDurationFromStart(start, TimeUnit::Microseconds);
//	cout << " microseconds!" << '\n';
//
//	//vector<string> targets = { "example_1_tar.fa" , "example_2_tar.fa" , "tar_test_1.fa" , "tar_test_4.fa" , "ref_test_2.fa" };
//	//vector<string> targets = { "2_tar_test_1.fa" };
//	//vector<string> targets = { "GRCh38_chr1.fna" };
//	vector<string> targets = { "GRCh38_chr1_fastaless.fna" };
//	//vector<string> targets = { "3_tar_3.fa" };
//
//	int counter = 1;
//	for (string& targetString : targets) {
//		compr.loadChromosome(targetString);
//		cout << "Loaded target!" << '\n';
//
//		string chrStr = fmt::format("chr{}.fa", counter);
//		cout << "Testing chromosome " << counter << '\n';
//		cout << "Target: " << targetString << '\n';
//
//		optional<reference_wrapper<Chromosome>> chrOpt = compr.getChromosome(chrStr);
//		if (chrOpt.has_value()) {
//			cout << fmt::format("Testing chromosome {}!", chrStr) << '\n';
//			Chromosome& chr = chrOpt.value();
//			vector<uint8_t>& reference = compr.getPrevRefSequence();
//			vector<uint8_t>& target = chr.getTargetSequence();
//
//			compr.compressChromosome(chrStr);
//			cout << "Compressed chromosome!" << '\n';
//
//			cout << "Compression time: ";
//			printDurationFromStart(start, TimeUnit::Microseconds);
//			cout << " microseconds!" << '\n';
//
//			bool success = decompr._restoreNumericSequence(chr);
//			if (!success) {
//				throw runtime_error("No compression data was present in order for the decompression to work!");
//			}
//
//			//cout << "Reference sequence >> " << '\n';
//			//cout << toString(reference, false) << '\n' << '\n';
//
//			//cout << "Target sequence >> " << '\n';
//			//cout << toString(target, false) << '\n';
//
//			vector<uint8_t>& restoredNumSeq = *decompr.getRestoredNumSequence(chrStr);
//			//cout << "Restored target numeric sequence >> " << '\n';
//			//cout << toString(restoredNumSeq, false) << '\n';
//
//			success = decompr._restoreCharacterSequence(chr);
//			if (!success) {
//				throw runtime_error("Error during decompression!");
//			}
//
//			success = decompr._restoreString(chr);
//			if (!success) {
//				throw runtime_error("Error during decompression!");
//			}
//
//			vector<char>& restoredCharSeq = *decompr.getRestoredCharSequence(chrStr);
//			string restoredString = *decompr.getRestoredString(chrStr);
//
//			//cout << "Restored target character sequence >> " << '\n';
//			//cout << toString(restoredCharSeq) << '\n';
//
//			//cout << "Restored string >> " << '\n';
//			//cout << restoredString << '\n';
//
//			compr.writeChr2File(chrStr);
//
//			cout << "Writing time: ";
//			printDurationFromStart(start, TimeUnit::Seconds);
//			cout << " seconds!" << '\n';
//
//			if (target == restoredNumSeq) {
//				cout << fmt::format("!!!!! Chromosome {} decompression successful! !!!!!", counter++) << '\n';
//			}
//			else {
//				cout << "!!!!! FAILURE FAILURE FAILURE !!!!!" << '\n';
//			}
//			cout << '\n';
//		}
//	}
//
//	cout << '\n';
//
//	// Zip it mate
//	compr.zipFolder();
//	cout << "Writing  and zipping time: ";
//	printDurationFromStart(start, TimeUnit::Seconds);
//	cout << " seconds!" << '\n';
//}
//
//void test2DecomprRef(string ref)
//{
//	// system("ls -al");
//	Compressor compr;
//	Decompressor decompr;
//
//	// Let's try decompression now
//	compr.loadRefFile(ref);
//	compr.preprocessRef();
//	cout << "Reference: " << ref << '\n' << '\n';
//
//	vector<string> targets = { "example_1_tar.txt" , "example_2_tar.txt" , "tar_test_1" , "tar_test_4" };
//
//	int counter = 1;
//	for (string& targetString : targets) {
//		compr.loadChromosome(targetString);
//		string chrStr = fmt::format("chr{}.fa", counter);
//		cout << "Testing chromosome " << counter << '\n';
//		cout << "Target: " << targetString << '\n';
//
//		optional<reference_wrapper<Chromosome>> chrOpt = compr.getChromosome(chrStr);
//		if (chrOpt.has_value()) {
//			cout << fmt::format("Testing chromosome {}!", chrStr) << '\n';
//			Chromosome& chr = chrOpt.value();
//			vector<uint8_t>& reference = compr.getPrevRefSequence();
//			vector<uint8_t>& target = chr.getTargetSequence();
//
//			compr.compressChromosome(chrStr);
//			bool success = decompr._restoreNumericSequence(chr);
//			if (!success) {
//				throw runtime_error("No compression data was present in order for the decompression to work!");
//			}
//
//			vector<uint8_t>& restored = *decompr.getRestoredNumSequence(chrStr);
//
//			cout << "Reference sequence >> " << '\n';
//			cout << toString(reference, false) << '\n' << '\n';
//
//			cout << "Target sequence >> " << '\n';
//			cout << toString(target, false) << '\n';
//
//			cout << "Restored target sequence >> " << '\n';
//			cout << toString(restored, false) << '\n';
//
//			if (target == restored) {
//				cout << fmt::format("!!!!! Chromosome {} decompression successful! !!!!!", counter++) << '\n';
//			}
//			else {
//				cout << "!!!!! FAILURE FAILURE FAILURE !!!!!" << '\n';
//			}
//			cout << '\n';
//		}
//	}
//
//	cout << '\n';
//}
//
//void test2DecomprRefChar(string ref)
//{
//	Compressor compr;
//	Decompressor decompr;
//
//	// Let's try decompression now
//	compr.loadRefFile(ref);
//	compr.preprocessRef();
//	cout << "Reference: " << ref << '\n' << '\n';
//
//	vector<string> targets = { "ref_test_1" ,"ref_test_2" ,"ref_test_3" 
//		, "tar_test_1" };
//
//	int counter = 1;
//	for (string& targetString : targets) {
//		compr.loadChromosome(targetString);
//		string chrStr = fmt::format("chr{}.fa", counter);
//		cout << "Testing chromosome " << counter << '\n';
//		cout << "Target: " << targetString << '\n';
//
//		optional<reference_wrapper<Chromosome>> chrOpt = compr.getChromosome(chrStr);
//		if (chrOpt.has_value()) {
//			cout << fmt::format("Testing chromosome {}!", chrStr) << '\n';
//			Chromosome& chr = chrOpt.value();
//			vector<uint8_t>& reference = compr.getPrevRefSequence();
//			vector<uint8_t>& target = chr.getTargetSequence();
//
//			compr.compressChromosome(chrStr);
//			bool success = decompr._restoreNumericSequence(chr);
//			if (!success) {
//				throw runtime_error("No compression data was present in order for the decompression to work!");
//			}
//
//			success = decompr._restoreCharacterSequence(chr);
//			if (!success) {
//				throw runtime_error("No compression data was present in order for the decompression to work!");
//			}
//
//			vector<uint8_t>& restored = *decompr.getRestoredNumSequence(chrStr);
//			vector<char>& restoredChars = *decompr.getRestoredCharSequence(chrStr);
//
//			cout << "Restored character target sequence >> " << '\n';
//			cout << toString(restoredChars) << '\n';
//
//			if (target == restored) {
//				cout << fmt::format("!!!!! Chromosome {} decompression successful! !!!!!", counter++) << '\n';
//			}
//			else {
//				cout << "!!!!! FAILURE FAILURE FAILURE !!!!!" << '\n';
//			}
//			cout << '\n';
//		}
//	}
//
//	cout << '\n';
//}
//
//void test1()
//{
//	Compressor compr;
//	Decompressor decompr;
//
//	compr.loadRefFile("ref_test_1.txt");
//
//	compr.loadRefFile("example_2_ref.txt");
//	compr.loadChromosome("example_2_tar.txt");
//	compr.preprocessRef();				// Construct hash table
//
//	bool success = compr.compressChromosome("chr1.fa");
//	if (!success) {
//		cout << "Ow" << '\n';
//	}
//	else {
//		cout << "Did something!" << '\n';
//	}
//	cout << "Debug point > Example 1" << '\n';
//
//	// Let's try decompression now
//	compr.loadRefFile("ref_test_4");
//	compr.loadChromosome("tar_test_4");
//}

// ####################################################				LEGACY TEST CODE			###################################################################
// ################################################################################################################################################################
// ####################################################				LEGACY TEST CODE			###################################################################