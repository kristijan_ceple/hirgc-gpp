import sys
import os
from pathlib import Path

import Levenshtein

chr_lengths = {}


def numberOfChars(path: str, name: str):
    with open(path, 'r') as file:
        data = file.read().strip()
        data = "".join(data.split())
        chr_lengths[name] = len(data)


genome_1 = sys.argv[1]
genome_2 = sys.argv[2]

# Do Cartesian
directory_genome_1 = os.fsencode(genome_1)
directory_genome_2 = os.fsencode(genome_2)

# First fill the dict
for genome_file in os.listdir(directory_genome_1):
    genome_filename = os.fsdecode(genome_file)
    # numberOfChars(genome_1 + '/' + genome_filename, genome_filename)
    chr_lengths[genome_filename] = Path(genome_1 + '/' + genome_filename).stat().st_size

for genome_file in os.listdir(directory_genome_2):
    genome_filename = os.fsdecode(genome_file)
    # numberOfChars(genome_2 + '/' + genome_filename, genome_filename)
    chr_lengths[genome_filename] = Path(genome_2 + '/' + genome_filename).stat().st_size

# for key in chr_lengths.keys():
#     chr_lengths[key] /= 1E5

means = []
intervals = []

# Move on to actually calculating
for genome_file_1 in os.listdir(directory_genome_1):
    genome_filename_1 = os.fsdecode(genome_file_1)

    minVal = 1 << 32            # unsigned int basically
    maxVal = -1
    mean_sum = 0
    mean_cnt = 0

    for genome_file_2 in os.listdir(directory_genome_2):
        genome_filename_2 = os.fsdecode(genome_file_2)
        dist = Levenshtein.distance(genome_1 + '/' + genome_filename_1, genome_2 + '/' + genome_filename_2)
        denominator = max(chr_lengths[genome_filename_1], chr_lengths[genome_filename_2])
        prctg = dist/denominator

        if prctg < minVal:
            minVal = prctg
        elif prctg > maxVal:
            maxVal = prctg
        mean_sum += prctg
        mean_cnt += 1

        # print(f"{genome_filename_1} <=> {genome_filename_2} : {prctg}")

    meanVal = mean_sum / mean_cnt
    print(f"Chromosome {genome_filename_1} >> Min: {minVal}, Max: {maxVal}, Mean: {meanVal}")
    means.append(meanVal)
    intervals.append(maxVal - minVal)

print("###############################################################################################################")
print("########################################           MEANS           ############################################")
print("###############################################################################################################")

for mean in means:
    print(mean)
print()

print("###############################################################################################################")
print("########################################           INTERVALS           ########################################")
print("###############################################################################################################")

for interval in intervals:
    print(interval)
print()

print("###############################################################################################################")
print("###############################################################################################################")
print("###############################################################################################################")

print(f"Max mean: {max(means)}")
print(f"Biggest interval: {max(intervals)}")

print("All done!")