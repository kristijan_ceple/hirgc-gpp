#include "Compressor.h"
#include "Util.h"
#include <iostream>
#include <filesystem>
#include <fstream>
#include <array>
#include <memory>
#include <chrono>
#include <boost/algorithm/string/trim.hpp>
#include <unordered_map>
using namespace std;
using namespace std::chrono;
namespace fs = std::filesystem;

Compressor::Compressor()
{
	if (PREDEFINED_SIZES) {
		// Initialise memory -- hashtable
		this->prevRefSequence.reserve(MAX_CHR_LEN);
	}

	array<uint8_t, CONST_K>& tupleCoeffsCached_All = this->tupleCoeffsCached_All;
	// Prepare the tuple coeffs
	for (uint8_t i = 1; i < CONST_K_MINUS_1; i++) {
		tupleCoeffsCached_All[i] = i << 1;
	}
	tupleCoeffsCached_All[CONST_K_MINUS_1] = (CONST_K - 1) << 1;
	this->tupleCoeffsCached_Last = tupleCoeffsCached_All[CONST_K_MINUS_1];

	TupleValue::initCoeffs(tupleCoeffsCached_All);
}

Chromosome* Compressor::loadChromosome(string path)
{
	// Check if more chromosomes than humans have(non-defective) have been called to construction
	try {
		Chromosome& toRet = this->chromosomes.emplace_back(path, this->prevRefSequence, this->defaultChromosomeNamesCounter++);
		return &toRet;
	}
	catch (runtime_error& e) {
		cerr << e.what() << '\n';
		return nullptr;
	}
	
}

Chromosome* Compressor::loadChromosome(string path, string name)
{
	try {
		Chromosome& toRet = this->chromosomes.emplace_back(path, this->prevRefSequence, name);
		return &toRet;
	}
	catch (runtime_error& e) {
		cerr << e.what() << '\n';
		return nullptr;
	}
}

bool Compressor::compressChromosome(std::string chromosomeName)
{
	// Find the chromosome first
	Chromosome* theOne = this->findChromosome(chromosomeName);

	if (!theOne) {
		return false;
	}

	// Now check if a reference is loaded, and whether the table has been constructed
	if (!this->refLoaded || !this->hashTableConstructed) {
		return false;
	}

	auto greedyStart = high_resolution_clock::now();
	// Finally, perform the search
	//theOne->greedyMatchingOptimised(this->hashTable, this->tupleCoeffsCached_All, this->tupleCoeffsCached_Last);
	//theOne->greedyMatching(this->hashTable);
	theOne->greedyMatchingOptimised(this->hashTable);
	auto greedyStop = high_resolution_clock::now();
	auto duration = duration_cast<seconds>(greedyStop - greedyStart);
	cout << "Greedy matching runtime: " << duration .count() << " seconds!" << '\n';

	return true;
}

bool Compressor::compressChromosome(Chromosome& toCompr)
{
	// Now check if a reference is loaded, and whether the table has been constructed
	if (!this->refLoaded || !this->hashTableConstructed) {
		return false;
	}

	auto greedyStart = high_resolution_clock::now();
	// Finally, perform the search
	//theOne->greedyMatchingOptimised(this->hashTable, this->tupleCoeffsCached_All, this->tupleCoeffsCached_Last);
	//theOne->greedyMatching(this->hashTable);
	toCompr.greedyMatchingOptimised(this->hashTable);
	auto greedyStop = high_resolution_clock::now();
	auto duration = duration_cast<seconds>(greedyStop - greedyStart);
	cout << "Greedy matching runtime: " << duration.count() << " seconds!" << '\n';

	return true;
}

bool Compressor::writeChr2File(Chromosome& toWrite, const std::string path)
{

	if (!toWrite.isGreedyMatchingRun()) {
		return false;
	}

	// Now let's write to file!
	ofstream writeFile(path + "/compr_" + toWrite.name + ".fa", ios::trunc);

	if (!writeFile.is_open()) {
		return false;
	}

	AUXVisitor auxVisitor{ writeFile };
	GreedyVisitor greedyVisitor{ writeFile };
	// A) Auxiliary data goes first

	// 1. Fasta identifirs
	writeFile << toWrite.fastaIdentifier << '\n';

	// 2. Comments
	for (Comment& comment : toWrite.comments) {
		auxVisitor(comment);
	}
	writeFile << '\n';

	// 3. Linebreak points
	toWrite.runLengthEncodingLBrks();
	for (unsigned int lbrkPoint : toWrite.lineBreakPositionsRunLength) {
		auxVisitor(lbrkPoint);
	}
	writeFile << '\n';

	// 4. Other characters, consist of pos, len as well
	for (OtherChar& othChar : toWrite.otherCharacters) {
		auxVisitor(othChar);
	}
	writeFile << '\n';

	// 5. N Intervals s
	for (Interval& nIntv : toWrite.NIntervals) {
		auxVisitor(nIntv);
	}
	writeFile << '\n';

	// 6. Lower Case Intervals
	for (Interval& lwcIntv : toWrite.lwcIntervals) {
		auxVisitor(lwcIntv);
	}
	writeFile << '\n';

	// B) Greedy matching results second!
	for (variant<Interval, uint8_t>& gmres : toWrite.greedyMatchingResults) {
		// Okay, the format is <pos,len ch pos,len ch ...>
		visit(greedyVisitor, gmres);
	}

	// The file has been written.
	writeFile.close();
	return true;
}

bool Compressor::writeChr2File(const std::string chromosomeName, const std::string path)
{
	// First check if such a chromosome even exists, and if it's compressed
	Chromosome* theOne = this->findChromosome(chromosomeName);

	if (!theOne) {
		return false;
	}

	return this->writeChr2File(*theOne, path);
}

bool Compressor::preprocessRef()
{
	if (!refLoaded) {
		return false;
	}

	// Now init the hash table
	this->initHashTable();

	auto preprocessStart = high_resolution_clock::now();

	// Initialise the set
	unsigned int n = this->prevRefSequence.size();

	// Cache
	vector<uint8_t>& refSequence = this->prevRefSequence;
	array<uint8_t, CONST_K>& tupleCoeffsCached = this->tupleCoeffsCached_All;
	vector<vector<unsigned int>*>& hashTable = this->hashTable;
	unsigned int n_minus_const_k = n - CONST_K;
	unsigned int lastCoeff = this->tupleCoeffsCached_Last;

	vector<unsigned int>** toCheck = nullptr;
	int entriesVal = -1;

	// Preprocess the 0th elements -> the first tuple
	unsigned long long sum = 0;
	for (uint8_t i = 0; i < CONST_K; i++) {
		sum += ((unsigned long long)refSequence[i]) << tupleCoeffsCached[i];
	}

	unsigned int hashValue = sum & HASH_TABLE_SIZE_MINUS_ONE;
	hashTable[hashValue] = new vector<unsigned int>(MIN_BUCKET_SIZE);

	// Got it, time to move on!
	for (unsigned int i = 1; i <= n_minus_const_k; i++) {
		// Delete the last element from the sum, divide by four, and add a new one to the sum
		sum -= refSequence[i - 1];		// Delete last(previous) element, coefficient is 1 either way(2^0)
		sum >>= 2;								// Divide by 4
		sum += ((unsigned long long)refSequence[i + CONST_K_MINUS_1]) << lastCoeff;		// Add last sum element by taking the numeric nucleotide multiplied by 2 to the power of lastCoeff
		hashValue = sum & HASH_TABLE_SIZE_MINUS_ONE;

		// Now add this tuple into the hash table
		// 1st check if a bucket already exists
		toCheck = &(hashTable[hashValue]);
		if (*toCheck != nullptr) {
			// Vector at that pos present already!
			(*toCheck)->push_back(i);
		}
		else {
			// No vector present at the pos! Make a new one!
			*toCheck = new vector<unsigned int>(MIN_BUCKET_SIZE);
			//(*toCheck)->reserve(MIN_BUCKET_SIZE);
			(*toCheck)->push_back(i);
		}
	}

	auto preprocessStop = high_resolution_clock::now();
	auto duration = duration_cast<seconds>(preprocessStop - preprocessStart);
	cout << "Hash table construction time: " << duration.count() << " seconds!" << '\n';

	this->hashTableConstructed = true;
	return true;
}