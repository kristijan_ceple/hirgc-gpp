import argparse
import os
import difflib
import filecmp

parser = argparse.ArgumentParser()
parser.add_argument("source", type=str,
                    help="1st directory")
parser.add_argument("target", type=str,
                    help="2nd directory")
parser.add_argument("-n", "--number", type=int,
                    help="Number of files for diff check",
                    default=48)

args = parser.parse_args()

numSet = False
if args.number == -1:
    numSet = True

# directory_genome_1 = os.fsencode(args.dir_1)
# directory_genome_2 = os.fsencode(args.dir_2)

d = difflib.Differ()
counter = 1
while True:
    source_filename = "chr" + str(counter) + ".fa"
    target_filename = "restored_compr_chr" + str(counter) + ".fa"

    # Now read both files in
    file_1_path = args.source + '/' + source_filename
    file_2_path = args.target + '/' + target_filename

    # with open(file_1_path, 'r') as file:
    #     file_1 = file.read()
    #
    # with open(file_2_path, 'r') as file:
    #     file_2 = file.read()

    # Now check them!
    result = filecmp.cmp(file_1_path, file_2_path, shallow=False)
    print(f"Comparing: {file_1_path} and {file_2_path}! Identity: {result}")

    counter += 1
    if counter > args.number:
        break
