import sys

if len(sys.argv) != 2:
    exit("Incorrect number of parameters!")

to_process = sys.argv[1]
print("Input string: " + to_process)


encoding_rule = {
    'A': '0',
    'C': '1',
    'G': '2',
    'T': '3'
}

output = ""
for char in to_process:
    output += encoding_rule[char]

print(output)