import os
import sys

if len(sys.argv) != 2:
    print("Invalid number of arguments! Please pass a fasta file!")
    exit(-1)

fileToSplit = sys.argv[1]
outputFolder = sys.argv[2]

