#pragma once
#include "Chromosome.h"
#include "Util.h"
#include "HashTable.h"
#include <vector>
#include <variant>
#include <array>
#include <chrono>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <optional>

class Compressor
{
private:
	std::vector<uint8_t> prevRefSequence;
	bool refLoaded = false;

	std::vector<Chromosome> chromosomes;
	std::array<uint8_t, CONST_K> tupleCoeffsCached_All;
	uint8_t tupleCoeffsCached_Last;

	// ####################			HASH TABLE			########################
	
	//					Legacy tries
	//std::unordered_map<unsigned int, std::vector<unsigned int>, 
	//	IdentityHasher, ValidComparator> hashTable;				// Vals are lists of tuples
	//std::vector<int> header;
	//std::vector<int> entries;

	std::vector<std::vector<unsigned int>*> hashTable;
	inline void initHashTable();
	bool hashTableConstructed = false;
	// ####################			HASH TABLE			########################

	uint8_t defaultChromosomeNamesCounter = 0;
	inline Chromosome* findChromosome(std::string chromosomeName);

	friend class DataHandler;
public:
	inline std::vector<uint8_t>& getPrevRefSequence();
	
	Compressor();
	inline bool loadRefFile(std::string path);
	bool preprocessRef();
	inline bool isRefLoaded();
	inline bool isHashTableConstructed();
	
	Chromosome* loadChromosome(std::string path);
	Chromosome* loadChromosome(std::string path, std::string name);

	bool compressChromosome(std::string chromosomeName);
	bool compressChromosome(Chromosome& toCompr);
	
	bool writeChr2File(const std::string name, const std::string path = comprPath);
	bool writeChr2File(Chromosome& toWrite, const std::string path = comprPath);
	inline void zipFolder(std::string toZipFolder = comprPath);
	inline std::optional<std::reference_wrapper<Chromosome>> getChromosome(std::string name);
};

inline bool Compressor::loadRefFile(std::string path)
{
	bool success = Util::loadRefFile(path, this->prevRefSequence);
	if (!success) {
		return false;
	}
	else {
		this->refLoaded = true;
		this->hashTableConstructed = false;
		return true;
	}
}

inline void Compressor::initHashTable()
{
	auto hashTableStart = std::chrono::high_resolution_clock::now();
	
	if (hashTable.size() > 0) {
		for (unsigned int i = 0; i < HASH_TABLE_SIZE; i++) {
			delete hashTable[i];
			hashTable[i] = nullptr;
		}
	}
	else {
		std::vector<std::vector<unsigned int>*>& hashTable = this->hashTable;			// Get the reference

		// Prepare the hash table
		hashTable.reserve(HASH_TABLE_SIZE);
		for (unsigned int i = 0; i < HASH_TABLE_SIZE; i++) {
			hashTable.push_back(nullptr);
		}
	}

	auto hashTableStop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::seconds>(hashTableStop - hashTableStart);
	std::cout << "Hash table preparation and memory initialisation: " << duration.count() << " seconds!" << '\n';
}

inline Chromosome* Compressor::findChromosome(std::string chromosomeName)
{
	Chromosome* theOne = nullptr;
	size_t n = this->chromosomes.size();
	for (int i = 0; i < n; i++) {
		theOne = &this->chromosomes[i];
		if (theOne->getName() == chromosomeName) {
			break;
		}
	}

	return theOne;
}

inline std::vector<uint8_t>& Compressor::getPrevRefSequence()
{
	return this->prevRefSequence;
}

inline bool Compressor::isRefLoaded()
{
	return this->refLoaded;
}

inline bool Compressor::isHashTableConstructed()
{
	return this->hashTableConstructed;
}

inline std::optional<std::reference_wrapper<Chromosome>> Compressor::getChromosome(std::string name)
{
	for (Chromosome& chr : this->chromosomes) {
		if (chr.getName() == name) {
			return chr;
		}
	}

	return std::nullopt;
}

inline void Compressor::zipFolder(std::string toZipFolder)
{
	// Time to compress it!
#if defined(_WIN64) || defined(_WIN32)
	std::string cmd = fmt::format(COMPR_CMD_7ZA_WIN_CUSTOM, toZipFolder, toZipFolder);
	system(cmd.c_str());
#elif defined(__unix__)
	std::string cmd = fmt::format(COMPR_FORMAT_STR, toZipFolder, toZipFolder);
	system(cmd.c_str());
#endif
}